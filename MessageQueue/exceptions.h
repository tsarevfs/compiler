#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <stdexcept>

class SocketError : public std::runtime_error
{
public:
    SocketError(std::string what)
        : std::runtime_error(what)
    {

    }
};

#endif // EXCEPTIONS_H
