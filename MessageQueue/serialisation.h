#ifndef COMMON_H
#define COMMON_H

#include <QtEndian>
#include <QByteArray>
#include <openssl/sha.h>

inline QByteArray serializedInt(qint32 value)
{
    QByteArray buffer;
    buffer.resize(sizeof(qint32));
    qToBigEndian<qint32>(value, reinterpret_cast<unsigned char *>(buffer.data()));
    return buffer;
}

inline qint32 deserealizedInt(QByteArray buffer)
{
    unsigned char * buff = (unsigned char *)buffer.data();
    return qFromBigEndian<qint32>(buff);
}

static const int HASH_LENGTH = SHA256_DIGEST_LENGTH;

inline QByteArray hash(QByteArray data)
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, data.data(), data.length());
    SHA256_Final(hash, &sha256);

    return QByteArray((char *)hash, SHA256_DIGEST_LENGTH);
}


#endif // COMMON_H
