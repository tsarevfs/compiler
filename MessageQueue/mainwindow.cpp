#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , messageQueue_(new MessageQueue(this))
{
    ui->setupUi(this);
    setWindowTitle("Server");

    ui->clientsView->setModel(messageQueue_->clientsModel());

}

MainWindow::~MainWindow()
{
    delete ui;
}
