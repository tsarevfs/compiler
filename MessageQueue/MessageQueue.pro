#-------------------------------------------------
#
# Project created by QtCreator 2014-04-25T16:08:08
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MessageQueue
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    messagequeue.cpp \
    mainwindow.cpp

HEADERS += \
    messagequeue.h \
    serialisation.h \
    requests.h \
    exceptions.h \
    mainwindow.h
QMAKE_CXXFLAGS += -std=c++0x

FORMS += \
    mainwindow.ui
