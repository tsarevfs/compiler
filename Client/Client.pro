#-------------------------------------------------
#
# Project created by QtCreator 2014-04-11T18:21:10
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    worker.cpp \
    client.cpp \
    queueclient.cpp

HEADERS  += mainwindow.h \
    worker.h \
    client.h \
    queueclient.h

LIBS += -lcrypto

FORMS    += mainwindow.ui
QMAKE_CXXFLAGS += -std=c++0x
