#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QByteArray>
#include <QMap>
#include <QDir>
#include <QThread>

#include "queueclient.h"

class Worker : public QObject
{
    Q_OBJECT
public:
    Worker(QObject *parent = 0);
    ~Worker();

signals:
    void put(QString queueName, QByteArray data);
    void subscribe(QString queueName);
    void pauseListen(bool pause);

public slots:
    void resultFromQueue(QByteArray data);

private:
    void compile(QByteArray fileHash, QByteArray fileData, QString &errorString);
    void compilationReady(QByteArray fileHash, QByteArray fileData);
    void compilationFailed(QByteArray fileHash, QString errorString);
    void startQueueClientThread();

private:
    QueueClient *queueClient_;
    QMap<QByteArray, QString> fileOwners_;
    QDir compileDir_;
    QThread *queueClientThread_;

};

#endif // WORKER_H
