#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "client.h"
#include "worker.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void buttonClicked();
    void workFailed(QString errorString);
    void workFinished(QMap<QByteArray, QByteArray> result);

    
private:
    Ui::MainWindow *ui;
    Client * client_;
    Worker * worker_;
    QString projectName_;
    QDir linkDir_;

};

#endif // MAINWINDOW_H
