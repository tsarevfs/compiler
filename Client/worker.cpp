#include "worker.h"
#include <QFile>
#include <QProcess>

#include "../MessageQueue/serialisation.h"
#include "../MessageQueue/requests.h"


Worker::Worker(QObject *parent) :
    QObject(parent)
  , queueClient_(new QueueClient(QHostAddress("192.168.104.227"), 55555, 55557))
  , queueClientThread_(new QThread(this))
{
    connect(queueClient_, SIGNAL(get(QByteArray)), this, SLOT(resultFromQueue(QByteArray)));
    connect(this, SIGNAL(put(QString,QByteArray)), queueClient_, SLOT(put(QString,QByteArray)));
    connect(this, SIGNAL(subscribe(QString)), queueClient_, SLOT(subscribe(QString)));
    connect(this, SIGNAL(pauseListen(bool)), queueClient_, SLOT(pauseListen(bool)));

    QString dataDirName = "compile";
    QDir dir(QDir::currentPath());
    dir.mkdir(dataDirName);
    dir.cd(dataDirName);
    compileDir_ = dir;

    startQueueClientThread();
}

Worker::~Worker()
{
    delete queueClient_;
}

void Worker::startQueueClientThread()
{
    QObject::connect(queueClientThread_, SIGNAL(started()),queueClient_, SLOT(start()));
    QObject::connect(queueClient_, SIGNAL(finished()), queueClientThread_, SLOT(terminate()));

    queueClient_->moveToThread(queueClientThread_);
    queueClientThread_->start();

    subscribe("works");
}

void Worker::resultFromQueue(QByteArray data)
{
    pauseListen(true);
    qDebug() << "resultFromQueue";
    int pos = 0;
    int nameSize = deserealizedInt(data.mid(pos, sizeof(qint32)));
    pos += sizeof(qint32);

    QString clientName = QString::fromUtf8(data.mid(pos, nameSize));
    pos += nameSize;

    QByteArray fileHash = data.mid(pos, HASH_LENGTH);
    pos += HASH_LENGTH;

    int fileSize = deserealizedInt(data.mid(pos, sizeof(qint32)));
    pos += sizeof(qint32);

    QByteArray fileData = data.mid(pos, fileSize);
    pos += fileSize;

    fileOwners_[fileHash] = clientName;

    QString errorString;
    compile(fileHash, fileData, errorString);
    pauseListen(false);
}

void Worker::compile(QByteArray fileHash, QByteArray fileData, QString &errorString)
{
    qDebug() << "compilation started";
    QString sourceName = fileHash.toHex() + ".cpp";
    QString objectName = fileHash.toHex() + ".o";

    QString filePath = compileDir_.absoluteFilePath(sourceName);
    QFile source(filePath);
    source.open(QIODevice::WriteOnly);
    source.write(fileData);
    source.close();

    QProcess gpp;
    gpp.setWorkingDirectory(compileDir_.absolutePath());
    gpp.setProcessChannelMode(QProcess::MergedChannels);
    QStringList arguments;
    arguments << "-c";
    arguments << (QString("-o") + objectName);
    arguments << sourceName;

    gpp.start("g++", arguments);

    if (!gpp.waitForStarted())
        return;

    if (!gpp.waitForFinished())
        return;

    QByteArray result = gpp.readAll();
    if (result.size() == 0)
    {
        QString objectFilePath = compileDir_.absoluteFilePath(objectName);
        QFile source(objectFilePath);
        source.open(QIODevice::ReadOnly);
        QByteArray objectFileData = source.readAll();
        source.close();
        compilationReady(fileHash, objectFileData);
    }
    else
    {
        errorString = QString::fromUtf8(result);
        qDebug() << errorString;
        compilationFailed(fileHash, errorString);
    }
    qDebug() << "compilation finished";

}

void Worker::compilationReady(QByteArray fileHash, QByteArray fileData)
{
    QByteArray result;

    result.append((char) WR_OK);

    result.append(fileHash);

    result.append(serializedInt(fileData.length()));
    result.append(fileData);
    put(fileOwners_[fileHash], result);
}

void Worker::compilationFailed(QByteArray fileHash, QString errorString)
{
    QByteArray result;

    result.append((char) WR_FAILED);
    result.append(errorString.toUtf8());
    put(fileOwners_[fileHash], result);
}
