#ifndef QUEUECLIENT_H
#define QUEUECLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QByteArray>


class QueueClient : public QObject
{
    Q_OBJECT
public:
    explicit QueueClient(const QHostAddress &serverAddress, int serverPort, int udpPort, QObject *parent = 0);
    ~QueueClient();
    
signals:
    void get(QByteArray data);
    void finished();
    
public slots:
    void start();
    void queueResponse();
    void queueDisconnected();
    void udpReadReady();
    void put(QString queueName, QByteArray data);
    void subscribe(QString queueName);
    void pauseListen(bool pause);

private:
    void sendGetRequest(QString queueName);

private:
    QTcpSocket *tcpSocket_;
    QUdpSocket *udpSocket_;
    QHostAddress serverAddress_;
    QString subscribtion_;

    QByteArray queueResponseBuffer_;
    int serverTcpPort_;
    int udpPort_;
    bool paused_;
};

#endif // QUEUECLIENT_H
