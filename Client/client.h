#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QSet>
#include <QPair>
#include <QByteArray>
#include <QThread>

typedef QList< QPair < QByteArray, QByteArray > > FileList;

#include "queueclient.h"

class Client : public QObject
{
    Q_OBJECT
public:
    Client(QString clientName, QObject *parent = 0);
    ~Client();
    void addWork(FileList fileList);
    
signals:
    void workFinished(QMap<QByteArray, QByteArray>);
    void workFailed(QString);
    void percentDone(int);
    void put(QString queueName, QByteArray data);
    void subscribe(QString queueName);
    void pauseListen(bool);

public slots:
    void resultFromQueue(QByteArray data);

private:
    QByteArray serializedWork(QByteArray fileData, QByteArray fileHash) const;
    void startQueueClientThread();

private:
    QString clientName_;
    QueueClient *queueClient_;

    QSet<QByteArray> pendingFiles_;
    QMap<QByteArray, QByteArray> compiledFiles_;
    QThread *queueClientThread_;
};

#endif // CLIENT_H
