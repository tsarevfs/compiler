#include "client.h"
#include "../MessageQueue/serialisation.h"
#include "../MessageQueue/requests.h"



Client::Client(QString clientName, QObject *parent) :
    QObject(parent)
  , clientName_(clientName)
  , queueClient_(new QueueClient(QHostAddress("192.168.104.227"), 55555, 55556))
  , queueClientThread_(new QThread(this))
{
    connect(queueClient_, SIGNAL(get(QByteArray)), this, SLOT(resultFromQueue(QByteArray)));
    connect(this, SIGNAL(put(QString,QByteArray)), queueClient_, SLOT(put(QString,QByteArray)));
    connect(this, SIGNAL(subscribe(QString)), queueClient_, SLOT(subscribe(QString)));
    connect(this, SIGNAL(pauseListen(bool)), queueClient_, SLOT(pauseListen(bool)));

    startQueueClientThread();
}

Client::~Client()
{
    delete queueClient_;
}

void Client::startQueueClientThread()
{
    QObject::connect(queueClientThread_, SIGNAL(started()), queueClient_, SLOT(start()));
    QObject::connect(queueClient_, SIGNAL(finished()), queueClientThread_, SLOT(terminate()));

    queueClient_->moveToThread(queueClientThread_);
    queueClientThread_->start();

    subscribe(clientName_);
}

void Client::addWork(FileList fileList)
{
    pendingFiles_.clear();
    compiledFiles_.clear();
    percentDone(0);
    foreach(auto const &file, fileList)
    {
        put("works", serializedWork(file.first, file.second));
        pendingFiles_.insert(file.first);
    }
}

void Client::resultFromQueue(QByteArray data)
{
    pauseListen(true);
    int resultCode = data[0];

    if (resultCode == WR_OK)
    {
        qDebug() << "work ok";

        int pos = 1;
        QByteArray fileHash = data.mid(pos, HASH_LENGTH);
        pos += HASH_LENGTH;

        int fileSize = deserealizedInt(data.mid(pos, sizeof(qint32)));
        pos += sizeof(qint32);

        QByteArray fileData = data.mid(pos, fileSize);
        pos += fileSize;

        if (pendingFiles_.contains(fileHash))
        {
            compiledFiles_[fileHash] = fileData;
            pendingFiles_.remove(fileHash);
            percentDone(100 * (double)compiledFiles_.size() / (pendingFiles_.size() + compiledFiles_.size()));
            if (pendingFiles_.empty())
                workFinished(compiledFiles_);
        }
    }
    if (resultCode == WR_FAILED)
    {
        qDebug() << "work failed";
        compiledFiles_.clear();
        pendingFiles_.clear();
        workFailed(QString::fromUtf8(data.mid(1)));
    }
    pauseListen(false);
}

QByteArray Client::serializedWork(QByteArray fileHash, QByteArray fileData) const
{
    QByteArray work;

    QByteArray rawName = clientName_.toUtf8();
    work.append(serializedInt(rawName.size()));
    work.append(rawName);

    work.append(fileHash);

    work.append(serializedInt(fileData.size()));
    work.append(fileData);

    return work;
}
