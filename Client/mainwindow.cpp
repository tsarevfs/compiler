#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QProcess>
#include <QFile>
#include <QMessageBox>
#include "../MessageQueue/serialisation.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    client_(new Client("wasya", this)),
    worker_(new Worker(this))
{
    ui->setupUi(this);
    setWindowTitle("Client");

    connect(ui->launchButton, SIGNAL(clicked()), this, SLOT(buttonClicked()));

    ui->progressBar->setValue(0);
    connect(client_, SIGNAL(percentDone(int)), ui->progressBar, SLOT(setValue(int)));

    connect(client_, SIGNAL(workFailed(QString)), this, SLOT(workFailed(QString)));
    connect(client_, SIGNAL(workFinished(QMap<QByteArray,QByteArray>)), this, SLOT(workFinished(QMap<QByteArray,QByteArray>)));

    QString dataDirName = "link";
    QDir dir(QDir::currentPath());
    dir.mkdir(dataDirName);
    dir.cd(dataDirName);
    linkDir_ = dir;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::buttonClicked()
{
    FileList fileList;

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("FileList (*.list)"));
    QFileInfo fileInfo(fileName);
    QDir fileDir = fileInfo.dir();
    QFile compileFile(fileName);
    if (compileFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&compileFile);
       projectName_ = in.readLine().trimmed();

       while ( !in.atEnd() )
       {
          QString line = in.readLine();
          QFile sourceFile(fileDir.absoluteFilePath(line.trimmed()));
          if (sourceFile.open(QIODevice::ReadOnly))
          {
              QByteArray fileData = sourceFile.readAll();
              sourceFile.close();

              fileList.append(qMakePair(hash(fileData), fileData));
          }
       }
       compileFile.close();
    }

    client_->addWork(fileList);
}

void MainWindow::workFailed(QString errorString)
{
    QMessageBox msgBox;
    msgBox.setText("Compilation failed");
    msgBox.setInformativeText(errorString);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();
}


void MainWindow::workFinished(QMap<QByteArray, QByteArray> result)
{
    qDebug("workFinished");
    QStringList arguments;
    foreach(QByteArray fileHash, result.keys())
    {
        QString objectName = fileHash.toHex() + ".o";
        QByteArray fileData = result[fileHash];

        arguments << objectName;
        QString filePath = linkDir_.absoluteFilePath(objectName);

        QFile objectFile(filePath);
        objectFile.open(QIODevice::WriteOnly);
        objectFile.write(fileData);
        objectFile.close();
    }


    QProcess gpp;
    gpp.setWorkingDirectory(linkDir_.absolutePath());
    gpp.setProcessChannelMode(QProcess::MergedChannels);
    arguments << (QString("-o") + projectName_);

    gpp.start("g++", arguments);

    if (!gpp.waitForStarted())
        return;

    if (!gpp.waitForFinished())
        return;

    QByteArray gppResult = gpp.readAll();

    if (gppResult.size() == 0)
    {
        QMessageBox msgBox;
        msgBox.setText("Success");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
    else
    {
        QString errorString = QString::fromUtf8(gppResult);
        QMessageBox msgBox;
        msgBox.setText("Linker failed");
        msgBox.setInformativeText(errorString);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
}
